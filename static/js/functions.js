(function ($) {
    'use strict';

    // Slider Activate
    if ($.fn.owlCarousel) {
        $(".testimonials").owlCarousel({
            items: 1,
            margin: 0,
            loop: true,
            nav: true,
            navText: ['<button class="slider-navigation slider-navigation_left" aria-label="navigate left"><img src="img/icons/left-arrow.svg" alt="navigate left"></button>', '<button class="slider-navigation slider-navigation_right" aria-label="navigate right"><img src="img/icons/right-arrow.svg" alt="nvaigate right"></button>'],
            dots: false,
            autoplay: true,
            smartSpeed: 700,
            autoplayTimeout: 5000
        });
    }

	// Slider Activate with Captions (figcaption)
    if ($.fn.owlCarousel) {
		var owl = $('.client_slides');
		owl.owlCarousel({
            items: 3,
            margin: 0,
            loop: true,
            nav: true,
            navText: [
	            '<button class="slider-navigation slider-navigation_left" aria-label="navigate left"><img src="img/icons/left-arrow.svg" alt="navigate left"></button>',
	            '<button class="slider-navigation slider-navigation_right" aria-label="navigate right"><img src="img/icons/right-arrow.svg" alt="nvaigate right"></button>'
	        ],
            dots: false,
			autoplay: true,
		    autoplayTimeout: 5000,
		    autoplayHoverPause: true,
            fluidSpeed: 1000,
            center: true,
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 1
                },
                576: {
                    items: 1
                },
                768: {
                    items: 2
                },
                992: {
                    items: 3
                }
            },
        });
	}

    // Onepage Nav Active Code
    if ($.fn.onePageNav) {
        $('header nav ul').onePageNav({
            currentClass: 'current_page_item',
            scrollSpeed: 1500,
            easing: 'easeInOutQuart'
        });
    }

    // Parallax active js
    if ($.fn.jarallax) {
        var isIE = /MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) || /Edge\/\d+/.test(navigator.userAgent);
        if (!isIE) {
            $('.parallax').jarallax({
                speed: 0.2,
                noIos: true,
            });
        }
    }

    if ($.fn.init) {
        new WOW().init();
    }

    // Counterup Active Code
    if ($.fn.counterUp) {
        $('.counter').counterUp({
            delay: 10,
            time: 2000
        });
    }

    // MatchHeight Active Code
    if ($.fn.matchHeight) {
        $('.item').matchHeight();
    }

    var $window = $(window);
    // Fullscreen Active Code    
    $window.on('resizeEnd', function () {
        $(".full_height").height($window.height());
    });
    $window.on('resize', function () {
        if (this.resizeTO) clearTimeout(this.resizeTO);
        this.resizeTO = setTimeout(function () {
            $(this).trigger('resizeEnd');
        }, 300);
    }).trigger("resize");

    // Sticky Active Code
    $window.on('scroll', function () {
        // Fadeout text code
        //$("main .slogan").css("opacity", 1 - $(window).scrollTop() / $('main .slogan').height());        
        
        if ($window.scrollTop() > 90) {
            $('.main').addClass('sticky fadeIn');
            $('body').addClass('mobile_menu_on fullmenu-on');
        } else {
            $('.main').removeClass('sticky fadeIn');
            $('body').removeClass('mobile_menu_on fullmenu-on');
        }
    });
})(jQuery);