#!/bin/bash
input=$1
input_size=`identify -format "%wx%h" $input`
input_format=`identify -format "%m" $input`
input_quality=`identify -format "%Q" $input`
output=$2 #output path
size=${3:-$input_size}
format=${4:-$input_format}
quality=${5:-$input_quality}

echo input path $input, output path $output, size $size, format $format, quality $quality

mkdir -p $output #make sure the folder exists
#mogrify -path $output -filter Triangle -define filter:support=2 -strip -resize $size -unsharp 0.25x0.08+8.3+0.045 -dither None -posterize 136 -strip -quality $quality -define jpeg:fancy-upsampling=off -define png:compression-filter=5 -define png:compression-level=9 -define png:compression-strategy=1 -define png:exclude-chunk=all -define webp:lossless=true -interlace none -colorspace sRGB -format $format $input
mogrify -path $output -strip -resize $size -quality $quality -colorspace sRGB -format $format $input