#!/bin/bash

##
# used imagemagick for jpg, png
# started with https://www.smashingmagazine.com/2015/06/efficient-image-resizing-with-imagemagick/
# adjusted with https://www.even.li/imagemagick-sharp-web-sized-photographs/
# used cwebp direct instead of imagemagick. significantly smaller output files
# https://web.dev/codelab-serve-images-webp/

# 2020-01-01 - simplified smush approach and generated webp from smushed jpg
##

# convert team photos to smaller jpg
./scripts/smush.sh 'static/img/team/*.*' 'static/img/team/300w' 300 jpg 40

# convert team photos to webp
for file in static/img/team/300w/*; do ./scripts/webp.sh $file "" photo; done;

# smush background images
./scripts/smush.sh 'static/img/bg-img/officemap.jpg' 'static/img/bg-img/400w' 400 jpg 50
./scripts/webp.sh 'static/img/bg-img/400w/officemap.jpg' "" drawing

# the following are background images and can't do srcset normally
# need something like https://aclaes.com/responsive-background-images-with-srcset-and-sizes/ 
# or to have them not as background images
./scripts/smush.sh 'static/img/bg-img/ocean.jpg' 'static/img/bg-img/800w' 800 jpg 50
./scripts/smush.sh 'static/img/bg-img/sail.jpg' 'static/img/bg-img/800w' 800 jpg 50
./scripts/smush.sh 'static/img/bg-img/sailing.jpg' 'static/img/bg-img/800w' 800 jpg 50

# nmsdc
./scripts/smush.sh 'static/img/core-img/nmsdc.png' 'static/img/core-img/350w' 350 jpg 50
./scripts/webp.sh 'static/img/core-img/350w/nmsdc.jpg' "" drawing

# scrum certs
# NOTE: smush png is making the files LARGER. am able to get a good webp by going from 300 to 200.
# ./scripts/smush.sh 'static/img/certs-img/*.*' 'static/img/certs-img/200w' 200 png 75
for file in static/img/certs-img/*; do ./scripts/webp.sh $file "static/img/certs-img/200w" icon 75 "200 200"; done;

# client images
./scripts/smush.sh 'static/img/partner-img/*.*' 'static/img/partner-img/475w' 475 jpg 90
#./scripts/smush.sh 'static/img/partner-img/*.*' 'static/img/partner-img/475w' 475 png 50
for file in static/img/partner-img/475w/*.jpg; do ./scripts/webp.sh $file 'static/img/partner-img/475w' picture 75; done;