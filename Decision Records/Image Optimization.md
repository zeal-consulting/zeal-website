# Image Optimization

## Research on whether we could use Cloudinary for our image typing and optimization

  ### Info
    Cloudinary is an image and video hosting site where you can upload your media assets and then fetch them via https. Its power comes in its simplicity and the ability to do conversions, filters, and optimizations via the request url of the media.

  - Example optimized image
    - [https://res.cloudinary.com/ecleptic/image/upload/c_scale,e_sharpen:100,f_auto,q_auto:best,w_80/v1579710252/badge-7242.png](https://res.cloudinary.com/ecleptic/image/upload/c_scale,e_sharpen:100,f_auto,q_auto:best,w_80/v1579710252/badge-7242.png)
    - can change/remove the parameter and it'll return

  #### Pros
  - Easy automatic conversion, just pull link like above ☝️
  - Extremely easy setup.

  #### Cons
  - Pricy if you want multiple uploads
    - Can upload with a simple post request though
  - Assets are separated from their source.




## Research on how to optimize images on the build step or whenever the user calls the script

  ### Info
    When the maintainer executes a script or runs build, there would be something like a make file that would run optimization scripts for images.

    The maintainer would potentially toss the images into a directory. And as long as it fits specifications, could automatically run whenever the build script is run.

  - Possible tools
    - [Kracken](https://www.kracken.io) Image optimization on build step
    - [CloudImage](https://docs.cloudimage.io/go/cloudimage-documentation-v7/en/introduction
)
  - Most people seem to use `npm pre-build` scripts to do things like image optimization

  #### Pros
  -  Images would be able to stay on git.
  -  After setup, would only need to run the build command for optimizations.

  #### Cons
  - There's no real way to get into Hugo's build script.
  - You would have to do NPM or something else to hook into the build
  - need to manually craft the image optimization scripts.
    - that comes with the potential difficulties of doing that wrong




## Just run a shell script on gitlab
  - Gitlab pipeline to do it automagically?

  ### Info
    After creating image optimization and conversion scripts, you can have gitlab run the optimization scripts and for media

    The more I think about this, the more I realize that this is pretty much the same as above but automatically on deploy

  #### Pros
  - Once set up, maintainers and new users wouldn't need to worry about running scripts when changing images.
  - All various image sizes and types would only be created on the pipeline potentially saving space on the repo.



  #### Cons
  - Setup would be difficult
  - Minimum of 3 scripts to run per image
  - May not work well enough automatically because we don't have enough
  - Potentially tied to gitLab.
